import unittest
from .tip_calculator import calculate_amount_per_person

class TipCalculatorTest(unittest.TestCase):
    def test_int_total(self):
        self.assertEqual(calculate_amount_per_person(150, 12, 5), '33.60')
    def test_float_total(self):
        self.assertEqual(calculate_amount_per_person(50.22, 15, 2), '28.88')

if __name__ == '__main__':
    unittest.main()
