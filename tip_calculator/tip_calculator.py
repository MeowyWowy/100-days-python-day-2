def calculate_amount_per_person(total_price, total_tip, total_people):
    return '%.2f'%((total_price / total_people) + (total_price / total_people) * (total_tip / 100))

if __name__ == '__main__':
    total_price = float(input("How much was the total bill?"))
    total_tip = int(input("What percentage was the tip?"))
    total_people = int(input("How many people were at dinner?"))
    print(f"Each person should pay ${calculate_amount_per_person(total_price, total_tip, total_people)}")